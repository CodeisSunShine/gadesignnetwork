//
//  GANAppDelegate.h
//  GADesignNetwork
//
//  Created by CodeisSunShine on 07/08/2019.
//  Copyright (c) 2019 CodeisSunShine. All rights reserved.
//

@import UIKit;

@interface GANAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
