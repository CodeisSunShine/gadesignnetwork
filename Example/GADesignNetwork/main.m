//
//  main.m
//  GADesignNetwork
//
//  Created by CodeisSunShine on 07/08/2019.
//  Copyright (c) 2019 CodeisSunShine. All rights reserved.
//

@import UIKit;
#import "GANAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GANAppDelegate class]));
    }
}
