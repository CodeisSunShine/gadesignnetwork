# GADesignNetwork

[![CI Status](https://img.shields.io/travis/CodeisSunShine/GADesignNetwork.svg?style=flat)](https://travis-ci.org/CodeisSunShine/GADesignNetwork)
[![Version](https://img.shields.io/cocoapods/v/GADesignNetwork.svg?style=flat)](https://cocoapods.org/pods/GADesignNetwork)
[![License](https://img.shields.io/cocoapods/l/GADesignNetwork.svg?style=flat)](https://cocoapods.org/pods/GADesignNetwork)
[![Platform](https://img.shields.io/cocoapods/p/GADesignNetwork.svg?style=flat)](https://cocoapods.org/pods/GADesignNetwork)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GADesignNetwork is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GADesignNetwork'
```

## Author

CodeisSunShine, myloveiscodejie@163.com

## License

GADesignNetwork is available under the MIT license. See the LICENSE file for more info.
